import csv
import numpy as np
import operator
from collections import Counter
from math import log
from random import random


class Node:
    def __init__(self):
        self.children = list()
        self.dataList = None
        self.attributeSplitOn = None
        self.attributeVal = ''
        self.attributeCol = -1
        self.label = None


class MetaData:
    rootList = list()
    attributes = dict()
    attributeList = list()
    trainingData = np.array(str)
    testData = np.array(str)
    labels = list()


def driver_function():
    trainFileName = './banks.csv'  
    testFileName = './banks-test.csv'
    numberOfTrees = 3 
    percentageOfAttributes = 0.5 
    print(f'number of trees: {numberOfTrees}')
    print(f'percentage of attributes {percentageOfAttributes}')

    read_training_data(trainFileName)
    read_test_data(testFileName)

    attrs = list(MetaData.attributes.keys())
    attrs = attrs[:-1]
    MetaData.attributeList = attrs

    train_and_test_random_forest(numberOfTrees, percentageOfAttributes)
    

def read_training_data(fileName):
    MetaData.trainingData = np.genfromtxt(fileName, delimiter=',',  dtype=str)
    #save attributes in a dict, where col:col_as_string
    MetaData.attributes = {key:MetaData.trainingData[0][key] for key in range(MetaData.trainingData.shape[1])}
    #remove top row (col titles) of data
    MetaData.trainingData = MetaData.trainingData[1:]

    MetaData.labels = list(Counter(MetaData.trainingData[:,-1]).keys())


def read_test_data(fileName): 
    MetaData.testData = np.genfromtxt(fileName, delimiter=',',  dtype=str)
    MetaData.testData = MetaData.testData[1:]


def train_and_test_random_forest(numberOfTrees, percentageOfAttributes):
    #train
    for i in range(numberOfTrees):
        attrs = random_sample(MetaData.attributeList, percentageOfAttributes)
        MetaData.rootList.append(Node())
        MetaData.rootList[i].data = MetaData.trainingData
        build_decision_tree(MetaData.rootList[i], attrs)

    #test 
    d1 = MetaData.testData.shape[0] + 1
    d2 = MetaData.testData.shape[1] + 1

    predictionArray = np.ndarray([d1,d2], dtype='object')
    predictionArray[1:,:-1] = MetaData.testData

    #add attribute titles to table
    attributes = list(MetaData.attributes.values())
    attributes.append('prediction')
    predictionArray[0,:] = attributes

    i = 1
    predictCorrect = 0
    for row in MetaData.testData:
        prediction = get_prediction(row)
        if prediction == row[-1]:
            predictCorrect = predictCorrect + 1
        predictionArray[i,-1] = prediction
        i = i + 1
    print(f'accuracy: {predictCorrect/(i-1)}')
    
    np.savetxt('predictions.csv', predictionArray, delimiter=',', fmt='%s')


def random_sample(attributes, percentageOfAttributes): 
    attributeList = []
    for a in attributes:
        if percentageOfAttributes < random(): 
            attributeList.append(a)

    return attributeList


def build_decision_tree(node, attributes: list):
    attr = attributes[:]
    if check_pure_node(node, attributes) or attr == []:
        node.label = get_majority_label(node.data) 
    else: 
        bestSplitAttr, partitions = get_best_split_attribute(node.data, attributes)
        node.children = list() 
        node.attributeSplitOn = bestSplitAttr
        uniqueAttributeValues = Counter(node.data[:,bestSplitAttr])

        for k in uniqueAttributeValues.keys():
            n = Node()
            n.attributeVal = k
            n.attributeCol = bestSplitAttr
            n.data = list() #list so data rows can be appended
            node.children.append(n)

        split_data(node, bestSplitAttr)
        attr.remove(bestSplitAttr)

        for p in range(partitions): 
            build_decision_tree(node.children[p], attr)


def get_majority_label(data): 
    labels = Counter(data[:,-1])
    temp = max(labels.items(), key=operator.itemgetter(1))[0]
    return temp


def get_best_split_attribute(data, attributes: list):
    uniqueValues = dict()
    totals = dict()

    for attr in attributes: 
        uniqueValues[attr] = Counter(data[:,attr])

    for attr in attributes:
        totals[attr] = sum(uniqueValues[attr].values())

    setEntropy = calculate_set_entropy(data, attributes)

    gainPerAttribute = {}
    for a in attributes:
        gainPerAttribute[a] = calculate_Gain(setEntropy, data, a)

    #https://stackoverflow.com/questions/268272/getting-key-with-maximum-value-in-dictionary
    attr = max(gainPerAttribute.items(), key=operator.itemgetter(1))[0]
    partitions = len(uniqueValues[attr].keys())
 
    return attr, partitions


def calculate_Gain(setEntropy, data, attribute): 
    attrEntropy = calculate_attribute_entropy(data, attribute)
    return setEntropy - attrEntropy
    
    
def calculate_set_entropy(data, attributes: list):
    labels = Counter(data[:,attributes[-1]])
    entropy = 0
    for v in labels.values():
        entropy = entropy - (v/data.shape[0]) * log(v/data.shape[0], 2)

    return entropy


def calculate_attribute_entropy(data, attribute):
    labels = MetaData.labels
    uniqueAttrValues = list(Counter(data[:,attribute]).keys())
    entropyInfo = {key:[0]*(len(labels)+1) for key in uniqueAttrValues}

    for row in data:
        key = row[attribute]
        entropyInfo[key][0] = entropyInfo[key][0] + 1
        for i in range(len(labels)):
            if row[-1] == labels[i]:
                entropyInfo[key][i+1] = entropyInfo[key][i+1] + 1

    attrEntropy = 0
    for v in entropyInfo.values(): 
        tempEntropy = 0
        for i in range(len(labels)): 
            if v[i+1] == 0:
                pass
            else:
                tempEntropy = tempEntropy - (v[i+1]/v[0]) * log(v[i+1]/v[0], 2)
        attrEntropy = attrEntropy + ((v[0]/data.shape[0]) * tempEntropy)

    return attrEntropy


def check_pure_node(node, attributes: list):
    labels = Counter(node.data[:,-1])
    if len(labels.keys()) == 1:
        return True 
    else: 
        return False


def split_data(parentNode, attribute): 
    for row in parentNode.data:
        for n in parentNode.children:
            if row[attribute] == n.attributeVal:
                n.data.append(list(row))
 
    #convert from list to array
    for n in parentNode.children:
        n.data = np.asarray(n.data)


def get_prediction(row):
    predictions = {key:0 for key in MetaData.labels}
    for r in MetaData.rootList:
        traverser = r
        while(True):
            label = traverser.label
            if label == None:
                for i in range(len(traverser.children)):
                    if traverser.children[i].attributeVal == row[traverser.attributeSplitOn]:
                        traverser = traverser.children[i]
                        break
            else: 
                predictions[label] = predictions[label] + 1
                break

    return max(predictions.items(), key=operator.itemgetter(1))[0]

if __name__ == "__main__":
    driver_function()
