# Random Forest
## Description:
A decision tree algorithm using Random Forests to predict the category for the given test data. 
10 decision trees are built with the probability of using an attribute in the process being set to 0.5. 
Information gain is used as the metric to decide what attribute is to be split on.

Completed October 2020 for CMPT 459. 
